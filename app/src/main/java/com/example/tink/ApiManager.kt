package com.example.tink

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import okhttp3.*
import java.io.IOException


class ApiManager(var postManager: PostManager) {
    private var client = OkHttpClient()

    fun requestPost(url: String, postEnum: PostEnum) {
        val request = Request.Builder()
            .url(url)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("ne10ina", "API call error")
                postManager.showErrorLayout(Error(ErrorEnum.ApiCall, postEnum))
            }
            override fun onResponse(call: Call, response: Response) {
                val res = response.body?.string()
                Log.e("ne10ina", res?:"API RES NULL")
                var jsonObject = Gson().fromJson(res, JsonObject::class.java)
                var jsonArray = (jsonObject["result"] as JsonArray)
                for(el in jsonArray) {
                    val post = Gson().fromJson(el.toString(), PostData::class.java)
                    postManager.addPost(post, postEnum)
                }
                postManager.incLinkNum(postEnum)
            }
        })
    }
}

