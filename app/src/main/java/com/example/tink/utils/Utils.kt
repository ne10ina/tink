package com.example.tink.utils

import android.content.res.Resources
import android.view.View
import android.view.animation.AlphaAnimation

class Utils {
    companion object {
        fun animateFadeOut(view: View) {
            val anim = AlphaAnimation(1.0f, 0.0f)
            anim.duration = 400
            view.startAnimation(anim)
        }

        fun dpToPx(dp: Int): Int = (dp * Resources.getSystem().displayMetrics.density).toInt()
    }
}