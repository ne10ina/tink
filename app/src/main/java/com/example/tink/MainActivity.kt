package com.example.tink

import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.tink.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var mHandler: Handler = Handler()
    var actionManager = ActionManager(this)
    private var errorInstance: Error? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun postProgressBarVisibility(state: Int) {
        mHandler.post(Runnable {
            if (state == View.GONE) {
                Utils.animateFadeOut(postLoadingBar)
                Handler().postDelayed({
                    postLoadingBar.visibility = state
                }, 400)
                return@Runnable
            }
            postLoadingBar.visibility = state
        })
    }

    fun onNextButtonClicked(view: View) {
        actionManager.nextPost()
    }

    fun onPrevButtonClicked(view: View) {
        actionManager.prevPost()
    }

    fun nextButtonEnabled(state: Boolean) {
        mHandler.post {
            nextButton.isEnabled = state
        }
    }

    fun prevButtonEnabled(state: Boolean) {
        mHandler.post {
            prevButton.isEnabled = state
        }
    }

    fun updatePostDescription(text: String) {
        mHandler.post {
            postDescription.text = text
        }
    }

    fun updatePostID(text: String) {
        mHandler.post {
            postID.text = text
        }
    }

    fun onInfoButtonClicked(view: View) {
        val alertDialog = AlertDialog.Builder(this@MainActivity).create()
        alertDialog.setTitle("Информация")
        alertDialog.setMessage("Разработчик: Игорь Лизунов\n\nFintech Tinkoff 2021\n\nСвязь Telegram: @ne10ina")
        alertDialog.setButton(
            AlertDialog.BUTTON_NEUTRAL, "Закрыть"
        ) { dialog, _ -> dialog.dismiss() }
        alertDialog.show()
    }

    fun onAccountButtonClicked(view: View) {
        Toast.makeText(this, "Show account activity", Toast.LENGTH_SHORT).show()
    }

    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            val checked = view.isChecked
            when (view.getId()) {
                R.id.radioLatest ->
                    if (checked) {
                        onPostTypeSelected(PostEnum.LATEST)
                    }
                R.id.radioTop ->
                    if (checked) {
                        onPostTypeSelected(PostEnum.TOP)
                    }
                R.id.radioHot ->
                    if (checked) {
                        showErrorLayout(Error(ErrorEnum.ApiCall, null))
                        return
                        // API Error: Section hot doesn't exist
                        Toast.makeText(this, "Ошибка API, раздела Hot не существует", Toast.LENGTH_SHORT).show()
                        radioLatest.isChecked = true
                        onPostTypeSelected(PostEnum.LATEST)
                    }
            }
        }
    }

    private fun onPostTypeSelected(postEnum: PostEnum) {
        actionManager.setCurrentPostType(postEnum)
        actionManager.currentPost()
    }

    fun onRefreshButtonClicked(view: View) {
        Utils.animateFadeOut(error)
        Handler().postDelayed({
            post.visibility = View.VISIBLE
            error.visibility = View.GONE
        }, 400)
        if(errorInstance?.errorEnum == ErrorEnum.ApiCall)
            retryApiCall()
        else if(errorInstance?.errorEnum == ErrorEnum.ImageLoading)
            retryImageLoading()
    }

    fun retryApiCall() {
        errorInstance?.postEnum?.let { actionManager.retryApiCall(it) }
    }

    fun retryImageLoading() {
        actionManager.currentPost()
    }

    fun showErrorLayout(errorEl: Error) {
        Utils.animateFadeOut(post)
        Handler().postDelayed({
            post.visibility = View.GONE
            error.visibility = View.VISIBLE
        }, 400)
        errorInstance = errorEl
    }
}