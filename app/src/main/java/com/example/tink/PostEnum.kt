package com.example.tink

enum class PostEnum(val link: String) {
    LATEST("https://developerslife.ru/latest/"),
    HOT("https://developerslife.ru/hot/"),
    TOP("https://developerslife.ru/top/")
}