package com.example.tink

import android.util.Log

class PostManager(private var actionManager: ActionManager) {
    private var postElementMap = mapOf(
        PostEnum.LATEST to Post(),
        PostEnum.TOP to Post(),
        PostEnum.HOT to Post()
    )
    var postEnum = PostEnum.LATEST
    private var firstRun = true
    private var apiManager = ApiManager(this)

    init {
        apiManager.requestPost(getLink(PostEnum.LATEST), PostEnum.LATEST)
        apiManager.requestPost(getLink(PostEnum.TOP), PostEnum.TOP)
        apiManager.requestPost(getLink(PostEnum.HOT), PostEnum.HOT)
    }

    fun addPost(postData: PostData, postEnum: PostEnum) {
        Log.e("ne10ina", "Added post to $postEnum. List size ${postElementMap[postEnum]!!.postList.size + 1}")
        postElementMap[postEnum]!!.postList.add(postData)
        actionManager.nextButtonEnabled(true)
        if (firstRun && postEnum == PostEnum.LATEST) {
            firstRun = false
            actionManager.currentPost()
        }
    }

    fun getCurrentPost(): PostData {
            return postElementMap[postEnum]!!.getItem()
    }

    fun getPrevPost(): PostData {
        if (isFirst())
            return getCurrentPost()
        incCurrentItem(postEnum, -1)
        return getCurrentPost()
    }

    fun getNextPost(): PostData {
        Log.i(
            "ne10ina",
            "Next post. CurrentItem =  ${postElementMap[postEnum]!!.currentItem}, PostList.size = ${postElementMap[postEnum]!!.postList.size}"
        )
        incCurrentItem(postEnum, 1)
        if (postElementMap[postEnum]!!.isCurrItemEqualsListSize()) {
            actionManager.nextButtonEnabled(false)
            apiManager.requestPost(getLink(postEnum), postEnum)
            return getPrevPost()
        }
        if (isLast()) {
            apiManager.requestPost(getLink(postEnum), postEnum)
        }
        return getCurrentPost()
    }

    fun isLast(): Boolean {
        return postElementMap[postEnum]!!.isLast()
    }

    fun isFirst(): Boolean {
        return postElementMap[postEnum]!!.isFirst()
    }

    private fun getLink(postEnum: PostEnum): String {
        return "${postEnum.link}${postElementMap[postEnum]!!.linkNum}?json=true"
    }

    fun incLinkNum(postEnum: PostEnum) {
        postElementMap[postEnum]!!.incLinkNumber()

    }

    fun incCurrentItem(postEnum: PostEnum, to: Int) {
        postElementMap[postEnum]!!.incCurrItem(to)
    }

    fun showErrorLayout(error: Error){
        actionManager.showErrorLayout(error)
    }

    fun retryApiCall(postEnum: PostEnum) {
        apiManager.requestPost(getLink(postEnum), postEnum)
        firstRun = true
        for(p in postElementMap) {
            if(p.value.postList.size == 0) {
                apiManager.requestPost(getLink(p.key), p.key)
            }
        }
    }
}