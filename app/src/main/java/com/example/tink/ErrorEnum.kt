package com.example.tink

enum class ErrorEnum {
    ApiCall,
    ImageLoading
}

class Error(
    var errorEnum: ErrorEnum,
    var postEnum: PostEnum?
)