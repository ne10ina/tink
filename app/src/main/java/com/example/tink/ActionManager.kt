package com.example.tink

import android.content.Context
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.tink.utils.Utils
import kotlinx.android.synthetic.main.activity_main.*

class ActionManager(private var context: Context) {
    private var postManager = PostManager(this)

    fun prevPost() {
        processUpdate(postManager.getPrevPost())
        if (postManager.isFirst())
            prevButtonEnabled(false)
    }

    fun currentPost() {
        processUpdate(postManager.getCurrentPost())
        if (postManager.isFirst())
            prevButtonEnabled(false)
    }

    fun nextPost() {
        processUpdate(postManager.getNextPost())
        if (!postManager.isFirst())
            prevButtonEnabled(true)
    }

    private fun processUpdate(postData: PostData) {
        (context as MainActivity).postProgressBarVisibility(View.VISIBLE)
        updateGlideImg(postData.gifURL ?: postData.previewURL)
        setPostViewDescription(postData.description)
        setPostViewID(postData.id)
    }

    private fun setPostViewDescription(text: String) {
        (context as MainActivity).updatePostDescription(text)
    }

    private fun setPostViewID(text: String) {
        (context as MainActivity).updatePostID("ID: $text")
    }

    private fun updateGlideImg(link: String?) {
        (context as MainActivity).mHandler.post {
            var requestOptions = RequestOptions()
            requestOptions =
                requestOptions.transforms(CenterCrop(), RoundedCorners(Utils.dpToPx(15)))
            Log.i("ne10ina", "Link to Gif $link")
            Glide.with(context as MainActivity)
                .load(link)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .apply(requestOptions)
                .listener(GlideLoadingListener(null, context))
                .into((context as MainActivity).postImage)
        }
    }

    fun nextButtonEnabled(state: Boolean) {
        (context as MainActivity).nextButtonEnabled(state)
    }

    fun prevButtonEnabled(state: Boolean) {
        (context as MainActivity).prevButtonEnabled(state)
    }

    fun setCurrentPostType(type: PostEnum) {
        postManager.postEnum = type
    }

    fun showErrorLayout(error: Error) {
        (context as MainActivity).mHandler.post{
            (context as MainActivity).showErrorLayout(error)
        }
    }

    fun retryApiCall(postEnum: PostEnum) {
        postManager.retryApiCall(postEnum)
    }
}