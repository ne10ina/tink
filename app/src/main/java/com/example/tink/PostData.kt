package com.example.tink

data class PostData(
    val id: String,
    val description: String,
    val gifURL: String,
    val gifSize: Int,
    val previewURL: String
)

class Post(
    var postList: MutableList<PostData> = mutableListOf<PostData>(),
    var currentItem: Int = 0,
    var linkNum: Int = 0
) {
    fun getItem(): PostData {
        return postList[currentItem]
    }

    fun isCurrItemEqualsListSize(): Boolean {
        return postList.size == currentItem
    }

    fun isLast(): Boolean {
        return currentItem == postList.size - 1
    }

    fun isFirst(): Boolean {
        return currentItem == 0
    }

    fun incLinkNumber() {
        linkNum += 1
    }

    fun incCurrItem(to: Int) {
        currentItem += to
    }
}