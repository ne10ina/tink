package com.example.tink

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.DrawableCrossFadeTransition

class GlideLoadingListener(private val callback: Callback? = null, private val context: Context) :
    RequestListener<Drawable> {

    interface Callback {
        fun onFailure(message: String?)
        fun onSuccess(dataSource: String)
    }

    override fun onLoadFailed(
        e: GlideException?,
        model: Any,
        target: Target<Drawable>,
        isFirstResource: Boolean
    ): Boolean {
        Log.e("ne10ina", "Image loading failed")
        (context as MainActivity).showErrorLayout(Error(ErrorEnum.ImageLoading, null))
        callback?.onFailure(e?.message)
        return false
    }

    override fun onResourceReady(
        resource: Drawable,
        model: Any,
        target: Target<Drawable>,
        dataSource: DataSource,
        isFirstResource: Boolean
    ): Boolean {
        callback?.onSuccess(dataSource.toString())
        target.onResourceReady(resource, DrawableCrossFadeTransition(1000, isFirstResource))
        (context as MainActivity).postProgressBarVisibility(View.GONE)
        return true
    }
}